package redblacktree

import (
	"math/rand"
	"strconv"
	"testing"
)

func Test_verify(t *testing.T) {
	rbt := &RedBlackTree{}
	rbt.root = &node{val: 2, black: true}
	rbt.root.right = &node{val: 2, black: false, parent: rbt.root}
	verifyTree(rbt, t)
}

func TestInsert(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}
	for _, v := range nums {
		rbt.Insert(v, "")
		verifyTree(rbt, t)
	}
}

func TestInsert_RandomVals(t *testing.T) {
	rbt := &RedBlackTree{}
	cnt := 0
	for cnt < 10000 {
		rbt.Insert(rand.Int63(), "")
		cnt++
		verifyTree(rbt, t)
		if nums, _ := rbt.GetAll(); len(nums) != cnt || len(nums) != rbt.Size() {
			t.Fail()
		}
	}
}

func verifyTree(rbt *RedBlackTree, t *testing.T) {
	if ok, _ := rbt.verify(); !ok {
		t.Fail()
	}
}

func TestDelete(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}
	for _, v := range nums {
		rbt.Insert(v, "")
		verifyTree(rbt, t)
	}
	for _, v := range nums {
		rbt.Delete(v)
		verifyTree(rbt, t)
	}
}

func TestDelete_NodeWith2RedDataNodes(t *testing.T) {
	rbt := &RedBlackTree{}
	rbt.root = &node{val: 2, black: true}
	rbt.root.left = &node{val: 1, black: false, parent: rbt.root}
	rbt.root.right = &node{val: 3, black: false, parent: rbt.root}
	verifyTree(rbt, t)
	rbt.Delete(int64(2))
	verifyTree(rbt, t)
}

func TestDelete_NodeWith2BlackDataNodes(t *testing.T) {
	rbt := &RedBlackTree{}
	rbt.root = &node{val: 2, black: true}
	rbt.root.left = &node{val: 1, black: true, parent: rbt.root}
	rbt.root.right = &node{val: 3, black: true, parent: rbt.root}
	verifyTree(rbt, t)
	rbt.Delete(int64(2))
	verifyTree(rbt, t)
}

func TestDelete_NodeWith2RedDataNodes_Deeper(t *testing.T) {
	rbt := &RedBlackTree{}
	rbt.root = &node{val: 4, black: true}
	rbt.root.left = &node{val: 2, black: false, parent: rbt.root}
	rbt.root.left.left = &node{val: 1, black: true, parent: rbt.root.left}
	rbt.root.left.right = &node{val: 3, black: true, parent: rbt.root.left}
	rbt.root.right = &node{val: 6, black: false, parent: rbt.root}
	rbt.root.right.left = &node{val: 5, black: true, parent: rbt.root.right}
	rbt.root.right.right = &node{val: 7, black: true, parent: rbt.root.right}
	verifyTree(rbt, t)
	rbt.Delete(int64(4))
	verifyTree(rbt, t)
}

func TestDelete_Random(t *testing.T) {
	rbt := &RedBlackTree{}
	for i := 0; i < 10000; i++ {
		rbt.Insert(int64(i), "")
	}
	cnt := 0
	for cnt < 10000 {
		ran := int64(rand.Intn(10000))
		if _, found := rbt.Find(ran); found {
			rbt.Delete(ran)
			cnt++
			verifyTree(rbt, t)
			if nums, _ := rbt.GetAll(); len(nums) != 10000-cnt || len(nums) != rbt.Size() {
				t.Fail()
			}
		}
	}
}

func TestGetAll(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	for _, v := range nums {
		rbt.Insert(v, "")
	}
	vals, _ := rbt.GetAll()
	for i, v := range vals {
		if i+1 != int(v) {
			t.Fail()
		}
	}
}

func TestFind(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	for _, v := range nums {
		rbt.Insert(v, "")
	}
	if _, ok := rbt.Find(11); ok {
		t.Fail()
	}
}

func TestFindEqualOrUpper(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	for _, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(v)))
	}
	if _, ok := rbt.FindEqualOrUpper(11); ok {
		t.Fail()
	}
}

func TestFindEqualOrUpper_FindVal(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{12, 10, 8, 6, 4, 2, 0}
	for _, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(v)))
	}
	if v, ok := rbt.FindEqualOrUpper(11); !ok {
		t.Fail()
	} else if v != "12" {
		t.Fail()
	}
}

func TestFindEqualOrNext_Pass10Keys_ExpectCorrectRange(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{313615023, 531934692, 945209127, 1007459898, 1695153053, 2305894850, 2891135131, 2966491056, 3200371159, 4177589656}
	for i, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(i)))
	}
	verifyTree(rbt, t)
	search := []int64{3680278493, 3293783307, 761092342, 3514404434, 1296910144, 786706273, 618036467, 1665522395, 1752942282, 4090115354}
	searchMap := map[string]int{}
	for _, v := range search {
		if data, ok := rbt.FindEqualOrUpper(v); ok {
			searchMap[data.(string)]++
		}
	}
	if searchMap["2"] != 3 || searchMap["4"] != 2 || searchMap["5"] != 1 || searchMap["9"] != 4 {
		t.Fail()
	}
}

func TestFindEqualOrNext_PassAnother10Keys_ExpectCorrectRange(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{2305894850, 1007459898, 3200371159, 1695153053, 4177589656, 945209127, 313615023, 2891135131, 531934692, 2966491056}
	for i, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(i)))
	}
	verifyTree(rbt, t)
	search := []int64{761092342, 4090115354, 3680278493, 786706273, 618036467, 3293783307, 1665522395, 1752942282, 3514404434, 1296910144}
	searchMap := map[string]int{}
	for _, v := range search {
		if data, ok := rbt.FindEqualOrUpper(v); ok {
			searchMap[data.(string)]++
		}
	}
	if searchMap["5"] != 3 || searchMap["3"] != 2 || searchMap["0"] != 1 || searchMap["4"] != 4 {
		t.Fail()
	}
}

func TestFindEqualOrLower(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	for _, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(v)))
	}
	if v, ok := rbt.FindEqualOrLower(11); !ok {
		t.Fail()
	} else if v != "10" {
		t.Fail()
	}
}

func TestFindEqualOrLower_FindNoVal(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	for _, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(v)))
	}
	verifyTree(rbt, t)
}

func TestFindEqualOrLower_Pass10Keys_ExpectCorrectRange(t *testing.T) {
	rbt := &RedBlackTree{}
	nums := []int64{2305894850, 1007459898, 3200371159, 1695153053, 4177589656, 945209127, 313615023, 2891135131, 531934692, 2966491056}
	for i, v := range nums {
		rbt.Insert(v, strconv.Itoa(int(i)))
	}
	verifyTree(rbt, t)
	search := []int64{761092342, 4090115354, 3680278493, 786706273, 618036467, 3293783307, 1665522395, 1752942282, 3514404434, 1296910144}
	searchMap := map[string]int{}
	for _, v := range search {
		if data, ok := rbt.FindEqualOrLower(v); ok {
			searchMap[data.(string)]++
		}
	}
	if searchMap["8"] != 3 || searchMap["1"] != 2 || searchMap["3"] != 1 || searchMap["2"] != 4 {
		t.Fail()
	}
}

func BenchmarkInsert(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for j := 0; j < 10000; j++ {
			rbt := &RedBlackTree{}
			rbt.Insert(int64(j), strconv.Itoa(j))
		}
	}
	b.StopTimer()
}

func BenchmarkDelete(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var rbt *RedBlackTree
		for j := 0; j < 10000; j++ {
			rbt = &RedBlackTree{}
			rbt.Insert(int64(j), strconv.Itoa(j))
		}
		for j := 0; j < 10000; j++ {
			rbt.Delete(int64(j))
		}
	}
	b.StopTimer()
}
