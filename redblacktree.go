package redblacktree

import (
	"errors"
)

// RedBlackTree ... supports int64 as key, string as data for now
type RedBlackTree struct {
	root  *node
	count int
}

type node struct {
	val    int64
	data   interface{}
	black  bool
	left   *node
	right  *node
	parent *node
}

// Size ... get number of nodes
func (t *RedBlackTree) Size() int {
	return t.count
}

// GetAll ... get all nodes as []int64, []string
func (t *RedBlackTree) GetAll() ([]int64, []interface{}) {
	nums, datas := []int64{}, []interface{}{}
	var dfs func(*node)
	dfs = func(root *node) {
		if root == nil {
			return
		}
		dfs(root.left)
		nums = append(nums, root.val)
		datas = append(datas, root.data)
		dfs(root.right)
	}
	dfs(t.root)
	return nums, datas
}

// Find ... find node with specified key, if found, return data, true, else return "", false
func (t *RedBlackTree) Find(val int64) (interface{}, bool) {
	n := dfs(t.root, val)
	if n != nil {
		return n.data, true
	}
	return nil, false
}

// FindEqualOrUpper ... find equal or upper node with specified key
func (t *RedBlackTree) FindEqualOrUpper(val int64) (interface{}, bool) {
	current := t.root
	var lastUpper *node
	for current != nil {
		if current.val == val {
			return current.data, true
		} else if current.val > val {
			lastUpper = current
			if current.left == nil {
				return current.data, true
			}
			current = current.left
		} else {
			if current.right == nil && lastUpper != nil && lastUpper.val > val {
				return lastUpper.data, true
			}
			current = current.right
		}
	}
	return nil, false
}

// FindEqualOrLower ... find equal or lower node with specified key
func (t *RedBlackTree) FindEqualOrLower(val int64) (interface{}, bool) {
	current := t.root
	for current != nil {
		if current.val == val {
			return current.data, true
		} else if current.val > val {
			current = current.left
		} else {
			if current.right == nil || current.right.val > val {
				return current.data, true
			}
			current = current.right
		}
	}
	return nil, false
}

// Insert ... insert z and data as node
func (t *RedBlackTree) Insert(z int64, data interface{}) {
	if t.root == nil {
		t.root = &node{val: z, data: data, black: true}
		t.count++
		return
	}
	compare := t.root
	for true {
		if z < compare.val {
			if compare.left == nil {
				break
			}
			compare = compare.left
		} else {
			if compare.right == nil {
				break
			}
			compare = compare.right
		}
	}
	znode := &node{val: z, data: data, parent: compare}
	t.count++
	if z < compare.val {
		compare.left = znode
	} else {
		compare.right = znode
	}
	t.fixCurrentNode(znode)
}

func (t *RedBlackTree) fixCurrentNode(znode *node) {
	zparent := znode.parent
	// 当前是根节点
	if zparent == nil {
		znode.black = true
		return
	} else if zparent.black {
		// 父节点是黑色节点
		return
	}
	// Z节点的父亲是祖父的左子节点
	if zparent == zparent.parent.left {
		if zparent.parent.right != nil && !zparent.parent.right.black {
			// Z节点的叔叔是红色节点
			zparent.black, zparent.parent.right.black, zparent.parent.black = true, true, false
			znode = zparent.parent
			t.fixCurrentNode(znode)
		} else if znode == zparent.left {
			// Z节点是左子节点，叔叔是黑色节点，祖父节点右旋
			zgrandpa := zparent.parent
			zgrandpaParent := zgrandpa.parent
			grandpaIsLeft := true
			if zgrandpaParent != nil && zgrandpa == zgrandpaParent.right {
				grandpaIsLeft = false
			}

			zparent.right, zgrandpa.left = zgrandpa, zparent.right
			zparent.right.parent = zparent
			if zgrandpa.left != nil {
				zgrandpa.left.parent = zgrandpa
			}
			zparent.black, zgrandpa.black = true, false

			if zgrandpaParent != nil {
				if grandpaIsLeft {
					zgrandpaParent.left = zparent
				} else {
					zgrandpaParent.right = zparent
				}
			} else {
				t.root = zparent
			}
			zparent.parent = zgrandpaParent
		} else {
			// Z节点是右子节点，叔叔是黑色节点，父亲节点坐旋
			zgrandpa := zparent.parent
			zparent.right, znode.left, zgrandpa.left = znode.left, zparent, znode
			if zparent.right != nil {
				zparent.right.parent = zparent
			}
			znode.left.parent = znode
			zgrandpa.left.parent = zgrandpa
			t.fixCurrentNode(zparent)
		}
	} else {
		if zparent.parent.left != nil && !zparent.parent.left.black {
			// Z节点的叔叔是红色节点
			zparent.black, zparent.parent.left.black, zparent.parent.black = true, true, false
			znode = zparent.parent
			t.fixCurrentNode(znode)
		} else if znode == zparent.right {
			// Z节点是右子节点，叔叔是黑色节点，祖父节点左旋
			zgrandpa := zparent.parent
			zgrandpaParent := zgrandpa.parent
			grandpaIsLeft := true
			if zgrandpaParent != nil && zgrandpa == zgrandpaParent.right {
				grandpaIsLeft = false
			}

			zparent.left, zgrandpa.right = zgrandpa, zparent.left
			zparent.left.parent = zparent
			if zgrandpa.right != nil {
				zgrandpa.right.parent = zgrandpa
			}
			zparent.black, zgrandpa.black = true, false

			if zgrandpaParent != nil {
				if grandpaIsLeft {
					zgrandpaParent.left = zparent
				} else {
					zgrandpaParent.right = zparent
				}
			} else {
				t.root = zparent
			}
			zparent.parent = zgrandpaParent
		} else {
			// Z节点是左子节点，叔叔是黑色节点，父亲节点右旋
			zgrandpa := zparent.parent
			zparent.left, znode.right, zgrandpa.right = znode.right, zparent, znode
			if zparent.left != nil {
				zparent.left.parent = zparent
			}
			znode.right.parent = znode
			zgrandpa.right.parent = zgrandpa
			t.fixCurrentNode(zparent)
		}
	}
}

func dfs(root *node, az int64) *node {
	if root == nil {
		return nil
	}
	if root.val == az {
		return root
	} else if root.val > az {
		return dfs(root.left, az)
	} else {
		return dfs(root.right, az)
	}
}

// Delete ... delete node with as key, if key does not exist, then ignore
func (t *RedBlackTree) Delete(az int64) {
	aznode := dfs(t.root, az)
	if aznode == nil {
		return
	}
	t.count--
	if aznode.left == nil && aznode.right == nil && aznode.parent == nil {
		t.root = nil
		return
	}
	// 第1种情况：az有1个非Nil子节点
	azparent := aznode.parent
	if aznode.left != nil && aznode.right == nil {
		anode := aznode.left
		if azparent == nil {
			t.root = anode
		} else {
			if azparent.left == aznode {
				azparent.left = anode
			} else {
				azparent.right = anode
			}
		}
		aznode.left, aznode.parent = nil, nil
		anode.parent = azparent
		anode.black = true
		return
	} else if aznode.right != nil && aznode.left == nil {
		anode := aznode.right
		if azparent == nil {
			t.root = anode
		} else {
			if azparent.left == aznode {
				azparent.left = anode
			} else {
				azparent.right = anode
			}
		}
		aznode.right, aznode.parent = nil, nil
		anode.parent = azparent
		anode.black = true
		return
	}
	// 第2种情况：az有0个非Nil子节点
	if aznode.left == nil && aznode.right == nil {
		// 2.1
		if !aznode.black {
			if azparent.left == aznode {
				aznode.parent = nil
				azparent.left = nil
			} else {
				aznode.parent = nil
				azparent.right = nil
			}
			return
		}
		aznodeIsLeft := aznode == azparent.left
		// 2.2 & 2.3 & 2.4 & 2.5 & 2.6
		aznode.parent = nil
		if aznodeIsLeft {
			azparent.left = nil
		} else {
			azparent.right = nil
		}
		t.fixCurrentDoubleBlackNode(aznode, azparent, aznodeIsLeft)
		return
	}
	// 第3种情况：az有2个非Nil子节点
	aznext := aznode.right
	for aznext.left != nil {
		aznext = aznext.left
	}
	if aznext.left == nil && aznext.right == nil {
		// 3.1
		aznextParentIsAznode := aznext.parent == aznode
		aznextBlack := aznext.black
		aznodeIsLeft := true
		if azparent != nil && azparent.right == aznode {
			aznodeIsLeft = false
		}

		aznextParent := aznext
		if !aznextParentIsAznode {
			aznextParent = aznext.parent
			aznext.parent.left = nil
		}

		aznext.left = aznode.left
		aznext.left.parent = aznext

		if !aznextParentIsAznode {
			aznext.right = aznode.right
			aznext.right.parent = aznext
		}

		aznode.left, aznode.right, aznode.parent = nil, nil, nil
		aznext.parent = azparent
		aznext.black = aznode.black
		if azparent != nil {
			if aznodeIsLeft {
				azparent.left = aznext
			} else {
				azparent.right = aznext
			}
		} else {
			t.root = aznext
		}
		if aznextBlack {
			t.fixCurrentDoubleBlackNode(nil, aznextParent, !aznextParentIsAznode)
		}
	} else {
		// 3.2
		aznextParentIsAznode := aznext.parent == aznode
		aznodeIsLeft := true
		if azparent != nil && azparent.right == aznode {
			aznodeIsLeft = false
		}

		aznextRight := aznext.right
		aznextRight.black = aznext.black
		if !aznextParentIsAznode {
			aznext.parent.left, aznextRight.parent = aznextRight, aznext.parent
		}

		aznext.left = aznode.left
		aznext.left.parent = aznext

		if !aznextParentIsAznode {
			aznext.right = aznode.right
			aznext.right.parent = aznext
		}

		aznode.left, aznode.right, aznode.parent = nil, nil, nil
		aznext.parent = azparent
		aznext.black = aznode.black
		if azparent != nil {
			if aznodeIsLeft {
				azparent.left = aznext
			} else {
				azparent.right = aznext
			}
		} else {
			t.root = aznext
		}
	}
}

func (t *RedBlackTree) fixCurrentDoubleBlackNode(aznode *node, azparent *node, aznodeIsLeft bool) {
	azbrother := azparent.left
	if aznodeIsLeft {
		azbrother = azparent.right
	}
	if azbrother.black {
		// 2.2
		// 2.2.1 & 2.2.2
		if aznodeIsLeft && (azbrother.left == nil && azbrother.right == nil || azbrother.left != nil && azbrother.left.black && azbrother.right != nil && azbrother.right.black) {
			if !azparent.black {
				azparent.black = true
				azparent.right.black = false
			} else {
				azparent.right.black = false
				if azparent.parent != nil {
					azparentIsLeft := true
					if azparent.parent.right == azparent {
						azparentIsLeft = false
					}
					t.fixCurrentDoubleBlackNode(azparent, azparent.parent, azparentIsLeft)
				}
			}
			return
		} else if !aznodeIsLeft && (azbrother.left == nil && azbrother.right == nil || azbrother.left != nil && azbrother.left.black && azbrother.right != nil && azbrother.right.black) {
			if !azparent.black {
				azparent.black = true
				azparent.left.black = false
			} else {
				azparent.left.black = false
				if azparent.parent != nil {
					azparentIsLeft := true
					if azparent.parent.right == azparent {
						azparentIsLeft = false
					}
					t.fixCurrentDoubleBlackNode(azparent, azparent.parent, azparentIsLeft)
				}
			}
			return
		}

		// 2.4
		if aznodeIsLeft && (azbrother.left == nil || azbrother.left != nil && azbrother.left.black) {
			// 2.4.1 & 2.4.3
			wnode := azparent.right
			w1node := wnode.left
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.left = wnode, azparent
			azparent.right = w1node
			if w1node != nil {
				w1node.parent = azparent
			}
			wnode.black, azparent.black = azparent.black, wnode.black
			wnode.right.black = true
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			return
		} else if !aznodeIsLeft && (azbrother.left == nil || azbrother.left != nil && azbrother.left.black) {
			// 2.4.2 & 2.4.4
			wnode := azparent.left
			w2node := wnode.right
			w2nodeLeft := w2node.left
			wnode.parent, w2node.left = w2node, wnode
			wnode.right = w2nodeLeft
			if w2nodeLeft != nil {
				w2nodeLeft.parent = wnode
			}
			w2node.black, wnode.black = wnode.black, w2node.black
			azparent.left, w2node.parent = w2node, azparent
			t.fixCurrentDoubleBlackNode(aznode, azparent, aznodeIsLeft)
			return
		}

		// 2.5
		if aznodeIsLeft && (azbrother.right == nil || azbrother.right != nil && azbrother.right.black) {
			// 2.5.1 & 2.5.3
			wnode := azparent.right
			w1node := wnode.left
			w1nodeRight := w1node.right
			wnode.parent, w1node.right = w1node, wnode
			wnode.left = w1nodeRight
			if w1nodeRight != nil {
				w1nodeRight.parent = wnode
			}
			w1node.black, wnode.black = wnode.black, w1node.black
			azparent.right, w1node.parent = w1node, azparent
			t.fixCurrentDoubleBlackNode(aznode, azparent, aznodeIsLeft)
			return
		} else if !aznodeIsLeft && (azbrother.right == nil || azbrother.right != nil && azbrother.right.black) {
			// 2.5.2 & 2.5.4
			wnode := azparent.left
			w2node := wnode.right
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.right = wnode, azparent
			azparent.left = w2node
			if w2node != nil {
				w2node.parent = azparent
			}
			wnode.black, azparent.black = azparent.black, wnode.black
			wnode.left.black = true
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			return
		}

		// 2.3
		if aznodeIsLeft && !azbrother.left.black && !azbrother.right.black {
			// 2.3.1 & 2.3.3
			wnode := azparent.right
			w1node := wnode.left
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.left = wnode, azparent
			azparent.right, w1node.parent = w1node, azparent
			wnode.black, azparent.black = azparent.black, wnode.black
			wnode.right.black = true
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			return
		} else if !aznodeIsLeft && !azbrother.left.black && !azbrother.right.black {
			// 2.3.2 & 2.3.4
			wnode := azparent.left
			w2node := wnode.right
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.right = wnode, azparent
			azparent.left, w2node.parent = w2node, azparent
			wnode.black, azparent.black = azparent.black, wnode.black
			wnode.left.black = true
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			return
		}
	} else {
		if aznodeIsLeft {
			wnode := azparent.right
			w1node := wnode.left
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.left = wnode, azparent
			azparent.right, w1node.parent = w1node, azparent
			wnode.black, azparent.black = azparent.black, wnode.black
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			t.fixCurrentDoubleBlackNode(aznode, azparent, aznodeIsLeft)
		} else {
			wnode := azparent.left
			w2node := wnode.right
			azgrandpa := azparent.parent
			azparentIsLeft := true
			if azgrandpa != nil && azparent == azgrandpa.right {
				azparentIsLeft = false
			}
			azparent.parent, wnode.right = wnode, azparent
			azparent.left, w2node.parent = w2node, azparent
			wnode.black, azparent.black = azparent.black, wnode.black
			if azgrandpa != nil {
				if azparentIsLeft {
					azgrandpa.left = wnode
				} else {
					azgrandpa.right = wnode
				}
			} else {
				t.root = wnode
			}
			wnode.parent = azgrandpa
			t.fixCurrentDoubleBlackNode(aznode, azparent, aznodeIsLeft)
		}
	}
}

func (t *RedBlackTree) verify() (bool, error) {
	// 1. 每一个节点的颜色不是红色就是黑色
	// 2. 根节点是黑色
	if t.root == nil {
		return true, nil
	}
	if !t.root.black {
		return false, errors.New("violate rule 2")
	}
	// 3. 每一个叶子节点（Nil，空节点，实际不存储数据）都是黑色的
	// 4. 红色节点的两个孩子都是黑色的
	// 5. 对于每一个节点，从它出发到它的后代叶子节点的所有简单路径上，黑色节点的数量都是相同的
	var dfs func(*node) (bool, error)
	blackHeight := 0
	dfs = func(root *node) (bool, error) {
		if !root.black && (root.left != nil && !root.left.black || root.right != nil && !root.right.black) {
			return false, errors.New("violate rule 4")
		}

		if root.left != nil && root.left.val >= root.val {
			return false, errors.New("violate BST")
		}

		if root.right != nil && root.right.val < root.val {
			return false, errors.New("violate BST")
		}

		if root.left == nil {
			cnt, current := 1, root
			for current != nil {
				if current.black {
					cnt++
				}
				current = current.parent
			}
			if blackHeight == 0 {
				blackHeight = cnt
			} else if blackHeight != cnt {
				return false, errors.New("violate rule 5")
			}
		} else if ok, err := dfs(root.left); !ok {
			return ok, err
		}

		if root.right == nil {
			cnt, current := 1, root
			for current != nil {
				if current.black {
					cnt++
				}
				current = current.parent
			}
			if blackHeight == 0 {
				blackHeight = cnt
			} else if blackHeight != cnt {
				return false, errors.New("violate rule 5")
			}
		} else if ok, err := dfs(root.right); !ok {
			return ok, err
		}

		return true, nil
	}
	return dfs(t.root)
}
